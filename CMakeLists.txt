cmake_minimum_required(VERSION 2.8.0)
set(CMAKE_CXX_FLAGS "-O3 -std=c++17")
project("RosquilleraFramworkReforged")
add_library(
  Rosquillera
  SHARED
  src/rf_asset.cpp
  src/rf_assetmanager.cpp
  src/rf_collision.cpp
  src/rf_debugconsole.cpp
  src/rf_engine.cpp
  src/rf_input.cpp
  src/rf_layer.cpp
  src/rf_map.cpp
  src/rf_primitive.cpp
  src/rf_process.cpp
  src/rf_soundmanager.cpp
  src/rf_structs.cpp
  src/rf_taskmanager.cpp
  src/rf_textmanager.cpp
  src/rf_time.cpp
  src/rf_window.cpp
  src/utils/jsoncpp.cpp
  src/utils/parser.cpp
)

target_include_directories(Rosquillera PUBLIC ./include/)
find_library(SDL2_LIBRARY SDL2)
target_link_libraries(
  Rosquillera
  PUBLIC
  ${SDL2_LIBRARY}
  SDL2main
  SDL2_image
  SDL2_mixer
  SDL2_net
  SDL2_ttf
)
