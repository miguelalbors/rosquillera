/*
  LA ROSQUILLERA FRAMEWORK - REFORGED
  Copyright (C) 2017 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#ifndef RF_INPUT_H
#define RF_INPUT_H

#include "rf_structs.h"
#include <SDL2/SDL.h>

namespace RF_Input
{
	namespace Private {
		extern SDL_Event event;
		extern const int JOYSTICK_DEAD_ZONE;
	};

	void Update();

	extern bool key[RF_Structs::_FOO_KEY];
	extern RF_Structs::RF_Mouse mouse;
	extern RF_Structs::Vector2<int> jDir;
	extern bool jkey[RF_Structs::_FOO_JKEY];
	extern bool jPlugged;

	extern SDL_Joystick *gGameController;

};

#endif // RF_INPUT_H
