<div style="text-align: center;">

  ![La rosquillera framework - Reforged](https://rosquillera.yawin.es/lib/tpl/dokuwiki/images/logo.png)
</div>

La Rosquillera Framework es un framework para hacer videojuegos con SDL2 que tiene como objetivo que el usuario pueda hacer juegos como rosquillas (o sea muchos, muy rápido y sin romperse la cabeza).

## ¿Qué ventajas ofrece respecto a usar SDL2 directamente?
Crear un videojuego no es sólo cuestión de pintar imágenes y capturar la entrada de teclado. Se necesita hacer uso de muchos sistemas adicionales que no proporciona esta librería. Por eso, la Rosquillera Framework provee de un juego de herramientas que, además de permitir al usuario abstraerse del uso de SDL2, ayuda en esas otras muchas tareas.

## ¿Por qué "Reforged"?
En 2015 comencé el desarrollo de un motor de videojuegos al que dí el nombre de “La Rosquillera”. Poco después, decidí que sería un framework por lo que decidí renombrarlo a “La Rosquillera Framework”.

Tras cursar la carrera de informática, decidí que este proyecto acarreaba muchos problemas de base que no podían arreglarse sin poner patas arriba todo el framework; por lo que decidí volver a implementarlo desde cero.

Esta nueva versión pasó a denominarse “La Rosquillera Framework - Reforged”.

## Dependencias
Estoy seguro de que se puede compilar para Windows, pero no soy capaz. Por eso, las dependencias que indico sólo están comprobadas para GNU/Linux:
 - libsdl2-2.0-0
 - libsdl2-dev
 - libsdl2-image-2.0-0
 - libsdl2-image-dev
 - libsdl2-mixer-2.0-0
 - libsdl2-mixer-dev
 - libsdl2-net-2.0-0
 - libsdl2-net-dev
 - libsdl2-ttf-2.0-0
 - libsdl2-ttf-dev

## Conceptos generales
El diseño de este framework busca facilitar al desarrollador muchas tareas engorrosas; como la carga y descarga de assets, la gestión de procesos, etc… Y, como en cualquier otro framework, es importante conocer el diseño del mismo para poder aprovechar al máximo sus capacidades.

Por eso, en esta sección explica los conceptos generales de diseño de La Rosquillera Framework - Reforged.

### Pipeline
Todas las herramientas para crear videojuegos proveen de un pipeline que organiza la ejecución de los procesos internos de la aplicación. Cuanto más grande y sofisticada es la herramienta, mayor y más complejo es su pipeline. Este framework no es tan grande ni sofisticado como otras soluciones; no es un motor de videojuegos, tan sólo una capa sobre las librerías base sobre las que crear un motor y, por tanto, el pipeline es más pequeño.

En este framework se proporciona una clase base (RF_Process) que será la clase que se integre con el pipeline, buscando que todas las clases que se desee integrar en él hereden de ella.

![Pipeline de La rosquillera](https://rosquillera.yawin.es/lib/exe/fetch.php?cache=&media=mypipeline.png)

El funcionamiento del pipeline es el siguiente:
1. Al instanciarse la clase se invocará al miembro Start.
2. Tantas veces como sea posible entre dos fotogramas, se invocará al miembro Update.
3. Justo antes de llamar al sistema de renderizado, se invocará al miembro Draw.
4. Durante el proceso de renderizado, cuando ya se hayan ordenado los procesos en el orden en el que se van a dibujar en pantalla, se invocará al miembro LateDraw. Esta función sólo será invocada si el objeto contiene un gráfico que pueda ser renderizado.
5. Al finalizar el proceso de renderizado se invocará al miembro AfterDraw. Esta función sólo será invocada si el objeto contiene un gráfico que pueda ser renderizado.
6. Al eliminar el objeto se invocará el miembro Dispose.

### Clases y espacios de nombres
Para poder llevar a cabo su trabajo, este framework dispone de toda una batería de clases y espacios de nombres que es importante conocer.

|Nombre      |Tipo       |Descripción |
| ---        | ---       | --- |
| RF_Asset | Clase | Clase de la que heredan todas las clases para gestionar recursos con el RF_AssetManager | gestor de assets |
| RF_AssetList | Clase | Clase que gestiona listas de assets |
| RF_AssetManager | Clase | Clase singleton que gestiona todos los assets de la aplicación |
| RF_AudioClip | Clase | Asset de música |
| RF_Collision | Namespace | Espacio de nombres que contiene las funciones necesarias para calcular las colisiones entre procesos |
| RF_DebugCommand | Clase | Estructura para crear comandos de debug |
| RF_DebugConsoleListener | Clase | Clase singleton que gestiona el debug |
| RF_Engine | Clase | Clase que controla el bucle principal de ejecución |
| RF_Font | Clase | Asset de fuente tipográfica |
| RF_FXClip | Clase | Asset de efecto sonoro |
| RF_Gfx2D | Clase | Asset de imagen 2D |
| RF_Input | Clase | Clase singleton que gestiona la entrada por teclado, ratón y mando |
| RF_Layer | Clase | Clase para realizar tareas de canvas |
| RF_Primitive | Clase | Espacio de nombres que contiene las funciones primitivas de pintado de píxeles |
| RF_Process | Clase | Clase base que debe heredar toda clase que quiera integrarse en el pipeline |
| RF_SoundManager | Clase | Clase singleton que gestiona el sistema de sonido |
| RF_Structs | Namespace | Espacio de nombres que contiene todas las estructuras de datos básicas del framework así como todos los enumerados y definiciones básicas |
| RF_TaskManager | Clase | Clase singleton que gestiona el pipeline |
| RF_TextManager | Clase | Clase singleton que gestiona la escritura de textos |
| RF_Time | Clase | Clase para el control del tiempo |
| RF_Window | Clase | Clase que contiene la lógica necesaria para hacer uso de las ventanas |
